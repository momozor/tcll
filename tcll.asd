(defsystem "tcll"
  :version "0.1.0"
  :author "momozor"
  :license "MIT"
  :depends-on ("cl-ppcre"
               "defenum"
               "parse-float"
               "parse-number")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "Turing (or Lambda Calculus equivalent) Complete Lambda Lookin"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "tcll/tests"))))

(defsystem "tcll/tests"
  :author ""
  :license ""
  :depends-on ("tcll"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for tcll"

  :perform (test-op (op c) (symbol-call :rove :run c)))
