# TCLL
just a simple lisp like language interpreter

## Spec
* supports variable binding, atoms (integer and symbols)
* supports + * arithmetics

## Usage
```
     "not defined yet"
     
```

## Prerequisites
* `sbcl` or `ccl`
* Quicklisp

## Installation
1. `cd quicklisp-path/local-projects/`
2. `git clone https://github.com/momozor/tcll`
3. `cd tcll`
4. Run `SLIME` or `sbcl`/`ccl` and type `(ql:quickload :tcll)`



## Javascript version
* if you got nodejs, run `js tcll.js`
* if you want to execute in browser, load the tcll.js into <script></script> element in an html file, and run in the browser. Make sure to check the browser console for output
