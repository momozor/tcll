(defpackage tcll/tests/main
  (:use :cl
        :tcll
        :rove))
(in-package :tcll/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :tcll)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
